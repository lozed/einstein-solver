import numpy as np
import matplotlib.pyplot as pp
from time import sleep

plot_dic={'interpolation':'nearest'}

d={0:{},1:{}}
a = np.random.uniform(size=(2,3,10,10))
for i,dic in d.iteritems():
    pp.figure(i+1)
    for j in range(3):
        pp.subplot(1,3,j+1)
        dic[j] = pp.imshow(a[i,j],**plot_dic)
        pp.axis('off')
        pp.title('%i, %i'%(i,j))

pp.show()

for _ in range(10):
    for i,dic in d.iteritems():
        for j in range(3):
            dic[j].set_data(a[i,(_+j)%3])
        pp.figure(i+1)
    sleep(0.5)
