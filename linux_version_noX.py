from puzzle import Clues, relations
from time import sleep
from numpy import where

########## general ##########

families = ['numbers', 'letters', 'romans',
            'dots', 'shapes', 'signs']

numbers = ['%i'%i for i in range(1,7)]
letters = ['a','b','c','d','e','f']
romans = ['%ii'%i for i in range(1,7)]
dots = ['%ipt'%i for i in range(1,7)]
shapes = ['th','tb','cr','ls','ph','pb']
signs = ['+','-','/','*','=','v']

items = [eval(f) for f in families]

all_items = []
for family in families:
    all_items += eval(family)

rels = [r[:3] for r in relations]

def item2duplet(item):
    for i in range(6):
        if item in eval(families[i]): f = i
    i = eval(families[f]).index(item)
    return (f,i)

########## plotter ##########

class BashPlotter:

    def __init__(self, puzzle):
        assert puzzle.N == 6,\
            'wrong puzzle size: %i'%puzzle.N
        self.puzzle = puzzle
        puzzle.assignGui(self)
        self.reset()

    def reset(self):
        self.lines = [' ',' ']
        
    def duplet2item(self, duplet):
        return items[duplet[0]][duplet[1]]

    def isOff(self, triplet):
        index = self.puzzle.nplet2index(triplet)
        return self.puzzle.off.spikes[index]
    
    def isOn(self, duplet):
        """
        duplet: (family, position)
        """
        f,p = duplet
        index = self.puzzle.nplet2index((f,-1,p))
        array = self.puzzle.on.spikes[index]
        if array.any():
            return where(array)[0][0]
        else: return None
    
    def offBlock(self, duplet):
        """
        plots a non-solved block
        duplet: (family, position)
        """
        f,p = duplet
        size = len(items[f][0])
        for i in range(3):
            if size<2:
                self.lines[0] += ' '
                self.lines[1] += ' '
            if not self.isOff((f,i,p)):
                self.lines[0] += self.duplet2item((f,i))
            else:
                self.lines[0] += size*' '
            if not self.isOff((f,i+3,p)):
                self.lines[1] += self.duplet2item((f,i+3))
            else:
                self.lines[1] += size*' '
            if size<3:
                self.lines[0] += ' '
                self.lines[1] += ' '

    def onBlock(self, duplet):
        """
        plots a solved block
        duplet: (family, position)
        """
        f,i = duplet
        size = len(items[f][0])
        self.lines[0] += 3*' '
        if size<2:
            self.lines[0] += ' '
        self.lines[0] += self.duplet2item((f,i))
        if size<3:
            self.lines[0] += ' '
        self.lines[0] += 3*' '
        self.lines[1] += 9*' '

    def vSeparatrix(self):
        # self.vBorder()
        print 71*'-'
        # self.vBorder()

    def hSeparatrix(self):
        self.lines[0] += ' | '
        self.lines[1] += ' | '
    
    def vBorder(self):
        line = 5*(11*' '+'|')
        print line

    def plot(self):
        # self.vBorder()
        for f in range(6):
            self.reset()
            for p in range(6):
                on = self.isOn((f,p))
                if 0 <= on < 6:
                    self.onBlock((f,on))
                else:
                    self.offBlock((f,p))
                if p<5:
                    self.hSeparatrix()
            print self.lines[0]
            print self.lines[1]
            if f<5:
                self.vSeparatrix()
        # self.vBorder()
        print ''
        sleep(0.1)

########## prompt ##########

class Prompter:

    str_general = '\nd: done       l: print items list        s: print current state\
    \n\n{ass[ign], nex[t], ani[sotrop], tri[plet], top}: enter relation '
    str_item = "\nenter item or press 'l' for items list "
    str_position = '\nenter position [1-6] '
    str_other = '\nenter (an)other item(s)? [Y,n] '

    def __init__(self, puzzle):
        assert puzzle.N == 6,\
            'wrong puzzle size: %i'%puzzle.N
        self.clues = Clues(puzzle)
    
    def getItem(self):
        item = None
        while not item:
            item = raw_input(self.str_item)
            if item == 'l' or item not in all_items:
                self.printList()
                item = None
        return item

    def getPosition(self):
        position = -1
        while position<0:
            position = int(raw_input(self.str_position))-1
            if position>5: position = -1
        return position

    def printList(self):
        print ''
        for family in families:
            print eval(family)

    def more(self):
        tmp = raw_input(self.str_other)
        if tmp == 'n': return False
        else: return True

    def general(self):
        tmp = raw_input(self.str_general)
        if tmp in relations:
            pass
        elif tmp in rels:
            tmp = relations[rels.index(tmp)]
        elif tmp == 'd':
            return False
        elif tmp == 'l':
            self.printList()
            return True
        elif tmp == 's':
            print ''
            self.clues.printAll()
            return True
        go = True
        while go:
            getattr(self, tmp)()
            go = self.more()
        return True

    def assign(self):
        item = self.getItem()
        p = self.getPosition()
        f,i = item2duplet(item)
        self.clues.assign((f,i,p))
    
    def next(self):
        item1 = self.getItem()
        item2 = self.getItem()
        self.clues.next((item2duplet(item1),item2duplet(item2)))

    def anisotrop(self):
        item1 = self.getItem()
        item2 = self.getItem()
        self.clues.anisotrop((item2duplet(item1),item2duplet(item2)))

    def top(self):
        item1 = self.getItem()
        item2 = self.getItem()
        self.clues.top((item2duplet(item1),item2duplet(item2)))

    def triplet(self):
        item1 = self.getItem()
        item2 = self.getItem()
        item3 = self.getItem()
        self.clues.triplet((item2duplet(item1),item2duplet(item2),item2duplet(item3)))

    def run(self):
        running = True
        while running: running = self.general()
        self.clues.build()
