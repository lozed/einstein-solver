from PyQt4.QtCore import SIGNAL, SLOT
from PyQt4.QtGui import QApplication, QWidget, \
    QLineEdit, QLabel, QHBoxLayout
import sys
 
if __name__=='__main__':
 
    app = QApplication(sys.argv)
 
    window = QWidget()
    window.setWindowTitle("arguments")
    layout = QHBoxLayout(window)
    line = QLineEdit()
    layout.addWidget(line)
    label = QLabel()
    layout.addWidget(label)
    line.connect(line, SIGNAL('textChanged(QString)'),
                 label, SLOT('setText(QString)'))
    window.show()
    # Show our window.
 
    app.exec_()
