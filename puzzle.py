# TODO Cell and Line same matrices ?
#      next and next2 redundant

import numpy as np
from itertools import product

########## general ##########

def matrixMult(M, v):
    return (M*v).sum(-1)

def diff(i, N):
    """
    returns a list from 0 to N-1 with i taken out
    """
    l = range(N)
    tmp = l.pop(i)
    return l

relations = ['assign', 'next', 'anisotrop', 'triplet', 'top']

########## puzzle class ##########

class Puzzle():

    def __init__(self, N):
        # puzzle size
        self.N = N
        # GUI
        self.gui = None

        # arrays

        # list containing all internal arrays
        # a cell is described by a triplet (family,item,position)
        self._arrays = []
        # ON neurons
        self.on = Array(self,(N,N,N))
        # OFF neurons
        self.off = Array(self,(N,N,N))
        # array checking if N-1 copies of a (family,item)
        # is OFF on a line
        line = Array(self,(N,N),N-1)
        # array checking if N-1 copies of a (family,position)
        # is OFF within a cell
        cell = Array(self,(N,N),N-1)

        # connections

        # list containing all connections
        self._connections = []
        # ON -> ON; OFF -> OFF; OFF -< ON
        _ = OnOff(self)
        # (f,i,-) -> line(f,i); line -> ON
        _ = Line(self,line)
        # (f,-,p) -> cell(f,p); cell -> ON
        _ = Cell(self,cell)

    def nplet2index(self, nplet, N = None):
        """
        returns index on flatten array from a n-uplet
        in a array of shape N
        nplet: (a_i) with 0 <= i < 3
        a_i >= 0 -- index
        a_i = -1 -- span
        N: specific shape (else puzzle size)
        return: index or [indices]
        0 <= ind < N**3 or Prod_i(N_i)
        """
        dim = len(nplet)
        if not N:
            N = [self.N for _ in range(dim)]
        ranges = ()
        coefs = [1]
        for i in range(dim)[::-1]:
            if i:
                coefs.append(coefs[-1]*N[i])
            if type(nplet[i])==list:
                ranges += (nplet[i],)
            elif nplet[i]>=0:
                ranges += ([nplet[i]],)
            elif nplet[i]==-1:
                ranges += (range(N[i]),)
        ind = []
        for i in product(*ranges):
            ind.append(sum([i[k]*coefs[k] for k in range(dim)]))
        ind.sort()
        return ind

    def index2triplet(self, n):
        """
        n: index -- 0 <= n < N**3
        return: triplet (family, item, position)
        """
        f,r = divmod(n,self.N**2)
        i,p = divmod(r,self.N)
        return f,i,p

    def assignGui(self, gui):
        """
        assigns GUI
        """
        self.gui = gui

    def regArray(self, array):
        """
        registers new array
        """
        self._arrays.append(array)

    def regConnection(self, connection):
        """
        registers new connection
        """
        self._connections.append(connection)

    def build(self, clues):
        for relation,nplets in clues.iteritems():
            for nplet in nplets:
                getattr(self, relation)(nplet)

    def assign(self, triplet):
        """
        assigns a triplet to a known position
        """
        self.on.turnOn(*triplet)

    def next(self, duplets):
        """
        duplet_i : (family, item)
        """
        shape = (2,self.N)
        # borders have a smaller threshold
        threshold = 2*np.ones(shape)
        threshold[:,[0,-1]] = 1
        # extra array with given shape
        sub = Array(self,shape,threshold)
        # connection sub -< sub; sub <-> OFF
        _ = Next(self,sub,duplets)

    def anisotrop(self, duplets):
        """
        duplet_i : (family, item)
        """
        shape = (2,self.N)
        # directed thresholds
        threshold = np.zeros(shape)
        threshold[0] = range(self.N)[::-1]
        threshold[1] = range(self.N)
        # extra array with given shape
        sub = Array(self,shape,threshold)
        # connection sub -< sub; sub <-> OFF
        _ = Aniso(self,sub,duplets)

    def triplet(self, duplets):
        """
        duplet_i : (family, item)
        """
        # center item not on the borders
        self.off.turnOn(*(duplets[1]+(0,)))
        self.off.turnOn(*(duplets[1]+(self.N-1,)))
        # 0 next to 1 and 1 next to 2
        self.next((duplets[0],duplets[1]))
        self.next((duplets[1],duplets[2]))
        # 0 two cells away from 2
        shape = (2,self.N)
        threshold = 2*np.ones(shape)
        threshold[:,[0,1,-2,-1]] = 1
        sub2 = Array(self,shape,threshold)
        con2 = Next2(self,sub2,(duplets[0],duplets[2]))
        # extra array with given shape
        sub = Array(self,self.N,2)
        # connection OFF(0,2) -> sub; sub -> OFF'(1)
        con = Triplet(self,sub,duplets)

    def top(self, duplets):
        """
        duplet_i : (family, item)
        """
        # connection ON -> ON; OFF -> OFF
        con = Top(self,duplets)

    def build(self, clues):
        """
        clues: {'assign': [(0,2,3), (1,1,5)],
                'next': [((3,2), (1,1))],
                'triplet': [((1,4), (5,5), (4,2))]} f.ex.
        """
        for relation, elements in clues.iteritems():
            for duplets in elements:
                getattr(self, relation)(duplets)

    def step(self):
        """
        executes one step of the dynamics
        """
        for connection in self._connections:
            connection.propagate()
        for array in self._arrays:
            array.evolve()

    def run(self):
        """
        runs dynamics
        """
        n = 0
        while self.on.spikes.sum()<self.N**2:
            self.step()
            n += 1
            if self.gui: self.gui.plot()
        _ = raw_input('finished in %i turn'%n)

########## arrays ##########

class Array:

    def __init__(self, puzzle, shape, threshold = 1):
        # registers array
        puzzle.regArray(self)
        self.puzzle = puzzle
        # membrane potential
        self.mem = np.zeros(shape, float).flatten()
        # spikes (thresholded membrane)
        self.spikes = self.mem.copy().astype(bool)
        self.shape = shape
        # size = shape of flattened array
        self.size = self.mem.shape
        if type(threshold) in [int,float]:
            self.threshold = threshold*np.ones(self.size)
        else:
            self.threshold = threshold.flatten()

    def evolve(self):
        """
        array one step evolution
        - thresholds membrane to get spikes
        - resets membrane
        """
        self.spikes = self.mem>=self.threshold
        self.mem *= 0

    def turnOn(self, family, item, position):
        """
        turns on triplet, only for arrays of shape (N,N,N)
        """
        assert self.shape == 3*(self.puzzle.N,),\
                             'turnOn: wrong array shape %s'%(self.shape,)
        self.spikes[self.puzzle.nplet2index((family,item,position))] = True

########## connections ##########

class Connection:

    # whether a connection matrix has been assigned
    assigned = False

    def __init__(self, puzzle, sub = None, matrix_creator = None):
        # registers connection
        puzzle.regConnection(self)
        self.puzzle = puzzle
        if sub: self.sub = sub
        if matrix_creator and not self.__class__.assigned:
            self.assign(matrix_creator)

    def assign(self, matrix_creator):
        """
        assigns a connection matrix
        """
        self.__class__.assigned = True
        self.__class__.matrix = matrix_creator(self.puzzle)

    def matrixProp(self, x, y):
        """
        propagates x spikes to y membranes via assigned matrix
        """
        y.mem += matrixMult(self.matrix,x.spikes)

    def copy(self, x, y, weight = 1, indx = None, indy = None):
        """
        copies x spikes to y membranes with weight
        and possibly only on certain indices
        """
        if indx and indy: y.mem[indy] += weight*x.spikes[indx]
        elif indx: y.mem += weight*x.spikes[indx]
        elif indy: y.mem[indy] += weight*x.spikes
        else: y.mem += weight*x.spikes

########## Matrix creators ##########

def matrixCreatorFactory(in_shape, out_shape, *args, **kwargs):
    def matrixCreator(func):
        def matrix(puzzle):
            N = puzzle.N
            mat = np.zeros(out_shape+in_shape, bool)
            if 'ind' in kwargs:
                ind = kwargs.pop('ind')
            else:
                ind = lambda nplet: puzzle.nplet2index(nplet)
            if 'range' not in kwargs:
                for f,i,p in product(range(N),range(N),range(N)):
                    func(N,mat,f,i,p,ind)
            else:
                _ = kwargs.pop('range')
                func(N,mat,ind,**kwargs)
            return mat
        return matrix
    return matrixCreator

### line ###

def lineMatrix(N, mat, f, i, p, ind):
    mat[ind((f,i)),ind((f,i,p))] = True

class Line(Connection):

    def __init__(self, puzzle, line):
        matrix_creator = matrixCreatorFactory(puzzle.off.size,line.size)(
            lineMatrix)
        Connection.__init__(self,puzzle,line,matrix_creator)

    def propagate(self):
        self.matrixProp(self.puzzle.off,self.sub)
        for p in range(self.puzzle.N):
            self.copy(self.sub,self.puzzle.on,
                      indy=self.puzzle.nplet2index((-1,-1,p)))

### cell ###

def cellMatrix(N, mat, f, i, p, ind):
    mat[ind((f,p)),ind((f,i,p))] = True

class Cell(Connection):

    def __init__(self, puzzle, cell):
        matrix_creator = matrixCreatorFactory(puzzle.off.size,cell.size)(
            cellMatrix)
        Connection.__init__(self,puzzle,cell,matrix_creator)

    def propagate(self):
        self.matrixProp(self.puzzle.off,self.sub)
        for i in range(self.puzzle.N):
            self.copy(self.sub,self.puzzle.on,
                      indy=self.puzzle.nplet2index((-1,i,-1)))

### on-off ###

def onoffMatrix(N, mat, f, i, p, ind):
    for k in diff(i,N):
        mat[ind((f,k,p)),ind((f,i,p))] = True
        mat[ind((f,p,k)),ind((f,p,i))] = True

class OnOff(Connection):

    def __init__(self, puzzle):
        matrix_creator = matrixCreatorFactory(puzzle.on.size,puzzle.off.size)(
            onoffMatrix)
        Connection.__init__(self,puzzle,matrix_creator=matrix_creator)

    def propagate(self):
        self.matrixProp(self.puzzle.on,self.puzzle.off)
        self.copy(self.puzzle.on,self.puzzle.on)
        self.copy(self.puzzle.off,self.puzzle.off)
        self.copy(self.puzzle.off,self.puzzle.on,-100)

### items ###

class ItemConnection(Connection):

    def __init__(self, puzzle, sub, items, matrix_creator = None):
        Connection.__init__(self,puzzle,sub,matrix_creator)
        self.items = []
        for item in items:
            self.items.append(self.puzzle.nplet2index(item+(-1,)))

### next ###

def nextMatrix(N, mat, ind, dist = 1):
    for i in range(N):
        for j in [i-dist, i+dist]:
            if 0<=j<N:
                mat[ind(0,i),ind(1,j)] = True
                mat[ind(1,i),ind(0,j)] = True
 
class Next(ItemConnection):

    def __init__(self, puzzle, sub, items):
        matrix_creator = matrixCreatorFactory(
            (2*puzzle.N,),(2*puzzle.N,),ind = lambda i,n: i*puzzle.N+n,dist = 1,range=True)(
                nextMatrix)
        ItemConnection.__init__(self,puzzle,sub,items,matrix_creator)

    def propagate(self):
        # within array
        self.matrixProp(self.sub,self.sub)
        # array <-> off
        self.copy(self.puzzle.off,self.sub,2,indx=self.items[0]+self.items[1])
        shape = self.sub.shape
        self.copy(self.sub,self.puzzle.off,
                  indx=self.puzzle.nplet2index((0,-1),shape),indy=self.items[0])
        self.copy(self.sub,self.puzzle.off,
                  indx=self.puzzle.nplet2index((1,-1),shape),indy=self.items[1])

class Next2(ItemConnection):

    def __init__(self, puzzle, sub, items):
        matrix_creator = matrixCreatorFactory(
            (2*puzzle.N,),(2*puzzle.N,),ind = lambda i,n: i*puzzle.N+n,dist = 2,range=True)(
                nextMatrix)
        ItemConnection.__init__(self,puzzle,sub,items,matrix_creator)

    def propagate(self):
        # within array
        self.matrixProp(self.sub,self.sub)
        # array <-> off
        self.copy(self.puzzle.off,self.sub,2,indx=self.items[0]+self.items[1])
        shape = self.sub.shape
        self.copy(self.sub,self.puzzle.off,
                  indx=self.puzzle.nplet2index((0,-1),shape),indy=self.items[0])
        self.copy(self.sub,self.puzzle.off,
                  indx=self.puzzle.nplet2index((1,-1),shape),indy=self.items[1])

### anisotrop ###

def anisoMatrix(N, mat, ind):
    for i in range(N-1):
        for j in range(i+1,N):
            mat[ind(1,j),ind(0,i)] = True
            mat[ind(0,i),ind(1,j)] = True

class Aniso(ItemConnection):

    def __init__(self, puzzle, sub, items):
        matrix_creator = matrixCreatorFactory(
            (2*puzzle.N,),(2*puzzle.N,),ind = lambda i,n: i*puzzle.N+n,range=True)(
                anisoMatrix)
        ItemConnection.__init__(self,puzzle,sub,items,matrix_creator)

    def propagate(self):
        # within array
        self.matrixProp(self.sub,self.sub)
        # array <-> off
        self.copy(self.puzzle.off,self.sub,self.puzzle.N-1,indx=self.items[0]+self.items[1])
        shape = self.sub.shape
        self.copy(self.sub,self.puzzle.off,
                  indx=self.puzzle.nplet2index((0,-1),shape),indy=self.items[0])
        self.copy(self.sub,self.puzzle.off,
                  indx=self.puzzle.nplet2index((1,-1),shape),indy=self.items[1])

### triplet ###

class Triplet(ItemConnection):

    def __init__(self, puzzle, sub, items):
        ItemConnection.__init__(self,puzzle,sub,items)

    def propagate(self):
        self.copy(self.puzzle.off,self.sub,indx=self.items[0])
        self.copy(self.puzzle.off,self.sub,indx=self.items[2])
        self.copy(self.sub,self.puzzle.off,indx=range(self.puzzle.N-1),indy=self.items[1][1:])
        self.copy(self.sub,self.puzzle.off,indx=range(1,self.puzzle.N),indy=self.items[1][:-1])

### top ###

class Top(ItemConnection):

    def __init__(self, puzzle, items):
        ItemConnection.__init__(self,puzzle,None,items)

    def propagate(self):
        self.copy(self.puzzle.on,self.puzzle.on,
                  indx=self.items[0]+self.items[1],indy=self.items[1]+self.items[0])
        self.copy(self.puzzle.off,self.puzzle.off,
                  indx=self.items[0]+self.items[1],indy=self.items[1]+self.items[0])

########## clues ##########

class Clues(dict):

    def __init__(self, puzzle):
        self.puzzle = puzzle
        self.reset()

    def reset(self):
        self.clear()
        self.update({r:[] for r in relations})
        
    def addRelation(self, relation, nplets):
        self[relation].append(nplets)

    def assign(self, triplet):
        self.addRelation('assign',triplet)

    def next(self, duplets):
        self.addRelation('next',duplets)

    def anisotrop(self, duplets):
        self.addRelation('anisotrop',duplets)

    def top(self, duplets):
        self.addRelation('top',duplets)

    def triplet(self, duplets):
        self.addRelation('triplet',duplets)

    def printAll(self, relation = None):
        if relation: print relation,self[relation]
        else:
            for relation in self.iteritems():
                print relation

    def build(self):
        self.puzzle.build(self)
