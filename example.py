items_list = [range(1,7), ['a','b','c','d','e','f'],
              ['%ii'%i for i in range(1,7)], \
              ['%ipt'%i for i in range(1,7)], \
              ['trg_h','trg_b','crr','lsng','ptgn_h','ptgn_b'], \
              ['+','-','/','*','=','sqrt']]

def convert(item):
    for i in range(6):
        if item in items_list[i]: s = i
    a = items_list[s].index(item)
    return (s, a)

def convert_dict(dic):
    dict = {}
    for type, lst in dic.iteritems():
        lst_tmp = []
        for rel in lst:
            if type == 'assign':
                lst_tmp.append(convert(rel[0])+(rel[1]-1,))
            else:
                tup = ()
                for i in range(len(rel)):
                    tup += (convert(rel[i]),)
                lst_tmp.append(tup)
        dict[type] = lst_tmp
    return dict

dict_1 = {}

dict_1['assign'] = ((0,5,2), (4,0,0), (4,1,5))
dict_1['next'] = (((2,1), (2,2)), ((2,0), (5,1)), ((4,4), (4,3)),
                  ((5,3), (2,2)))
dict_1['anisotrop'] = (((3,0), (5,5)), ((1,3), (2,3)), ((1,5), (1,0)),
                       ((1,4), (0,4)), ((3,4), (1,3)), ((2,3), (5,4)),
                       ((0,0), (3,1)))
dict_1['triplet'] = (((1,2), (3,2), (4,2)), ((0,4), (5,1), (5,2)),
                     ((4,4), (1,2), (0,2)), ((5,0), (0,2), (3,3)),
                     ((2,4), (2,3), (4,4)))
dict_1['top'] = (((0,0), (5,3)), ((0,3), (5,0)), ((3,4), (1,1)), ((5,0), (3,4)))

dict_2 = {}

dict_2['assign'] = [('+',3)]
dict_2['next'] = [(2,3), ('lsng','5i'), ('4i','trg_b'), ('3pt',3)]
dict_2['anisotrop'] = [('6i','-'), (4,'b'), ('=','1i'), ('ptgn_h','1pt'),
                       ('trg_h','ptgn_b')]
dict_2['triplet'] = [(5,'6i','+'), ('3pt','5pt','f'),
                     ('3i','b','e'), ('6pt',5,'ptgn_h'), \
                     ('a','c','4i'), ('1pt','2pt','3pt'), \
                     ('trg_b',3,'e')]
dict_2['top'] = [('crr','-'), ('lsng','/'), ('*',6)]

dict_3 = {}

dict_3['assign'] = [('2pt',4)]
dict_3['next'] = [('5i','trg_h'), ('6i','a'), ('c','2pt'), ('trg_h','*'),
                  ('5pt','6i'), (4,2), (6,'*')]
dict_3['anisotrop'] = [('4pt','sqrt'), ('a','+'), ('c','lsng'), ('d','2pt'),
                       ('lsng','crr')]
dict_3['triplet'] = [(2,'lsng','e'), ('b','3i','ptgn_h'),
                     ('1i','sqrt','ptgn_h'), ('=',3,'d'), \
                     (1,'d','2i'), ('3i','e','1pt')]
dict_3['top'] = [('ptgn_b','3pt'), (3,'-')]

dict_4 = {}

dict_4['assign'] = [('a',3), ('ptgn_b', 2)]
dict_4['next'] = [('5i',1), ('trg_h','6pt'), ('3pt','b'), ('3pt','/'),
                  ('3i','1pt')]
dict_4['anisotrop'] = [('3pt','5i'), ('1i','6i'), ('5i','2pt'), (3,'1pt'),
                       (2,'ptgn_h'), ('2i','a')]
dict_4['triplet'] = [('d',6,'1pt'), ('trg_b','-',5),
                     ('6i','5pt',2), ('=','3pt','4i')]
dict_4['top'] = [(2,'*'), (2,'e'), ('d','trg_b'), ('+','lsng'), ('c','1i')]

dict_5 = {}

dict_5['assign'] = [('*', 3)]
dict_5['next'] = [('e', '2'), ('1i', '2i'), ('sqrt', '5i'), ('1i', '3i'),
                  ('1', '-'), ('5i', 'lsng')]
dict_5['anisotrop'] = [('2pt', '=')]
dict_5['triplet'] = [('a', '4pt', '3'), ('sqrt', 'trg_b', '2'),
                     ('5pt', '*', '3'), ('e', '5', 'd'), ('=', '6i', '6'),\
                     ('5pt', '4pt', '/'), ('3', '2i','5')]
dict_5['top'] = [('c', '6pt'), ('d', 'trg_h'), ('4', '1pt'), ('crr', '-'),
                 ('f', '1i'), ('2pt', 'ptgn_h'), ('6pt', 'crr')]

dict_einst = {}

dict_einst['assign'] = [(2, 2, 2), (1, 4, 0)]
dict_einst['next'] = [((0, 1), (0, 3)), ((3, 0), (4, 1)), ((4, 4), (3, 2)),
                      ((1, 4), (0, 0)), ((3, 0), (2, 4))]
dict_einst['anisotrop'] = [((0, 1), (0, 3))]
dict_einst['triplet'] = []
dict_einst['top'] = [((1, 1), (0, 2)), ((1, 3), (4, 2)), ((1, 0), (2, 3)),
                     ((0, 1), (2, 1)), ((3, 3), (4, 0)), ((0, 4), (3, 2)),\
                     ((3, 1), (2, 0)), ((1, 2), (3, 4))]
